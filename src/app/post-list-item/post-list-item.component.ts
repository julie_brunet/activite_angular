import { Component, OnInit, Input } from '@angular/core';
import { PostService } from '../services/post.service'
@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {

   @Input() title: string;
   @Input() content: string;
   @Input() loveIts: number;
   @Input() date: string;
   @Input() id: number;

  constructor(private postService: PostService) {}

  ngOnInit() {
  }
  
  onLike(){
    this.postService.like(this.id)
  }

  onDislike(){
    this.postService.dislike(this.id)
  }

  onRemovePost(id: number){
    this.postService.removePost(id)
  }
}
