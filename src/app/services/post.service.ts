import { Subject } from 'rxjs';

export class PostService {
   
	postsSubject = new Subject<any[]>();
   posts = [
		{
			id: 0,
			title: 'post 1',
			content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec velit rutrum, commodo mi a, blandit metus. Nam faucibus, mauris et ullamcorper faucibus, tortor metus auctor ligula, a pretium nunc sem nec quam. Nullam varius et elit eu venenatis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec malesuada elit sit amet semper auctor. Morbi pulvinar libero id mi tristique, a porta elit venenatis. Integer a faucibus magna. Aliquam scelerisque sapien nec felis scelerisque tincidunt. Fusce sed dui auctor, ornare dolor a, varius purus. Vivamus id enim dolor. ',
  			loveIts: 0,
  			date: new Date()
		},
		{
			id: 1,
			title: 'post 2',
			content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec velit rutrum, commodo mi a, blandit metus. Nam faucibus, mauris et ullamcorper faucibus, tortor metus auctor ligula, a pretium nunc sem nec quam. Nullam varius et elit eu venenatis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec malesuada elit sit amet semper auctor. Morbi pulvinar libero id mi tristique, a porta elit venenatis. Integer a faucibus magna. Aliquam scelerisque sapien nec felis scelerisque tincidunt. Fusce sed dui auctor, ornare dolor a, varius purus. Vivamus id enim dolor. ',
  			loveIts: 0,
  			date:  new Date()
		},
		{
			id: 2,
			title: 'post 3',
			content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec velit rutrum, commodo mi a, blandit metus. Nam faucibus, mauris et ullamcorper faucibus, tortor metus auctor ligula, a pretium nunc sem nec quam. Nullam varius et elit eu venenatis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec malesuada elit sit amet semper auctor. Morbi pulvinar libero id mi tristique, a porta elit venenatis. Integer a faucibus magna. Aliquam scelerisque sapien nec felis scelerisque tincidunt. Fusce sed dui auctor, ornare dolor a, varius purus. Vivamus id enim dolor. ',
  			loveIts: 0,
  			date:  new Date()
		}
	];

	  emitPostSubject() {
    	this.postsSubject.next(this.posts.slice());
  }

	removePost(id: number) {
		var response = confirm("Etes vous sur de vouloir supprimer le post ?")
	   	if(response === true)
	   	{
	   		for (var i = 0; i < this.posts.length; ++i) {
	    		 if(this.posts[i].id === id){
	    	 		this.posts.splice(i, 1);
	    	 	}
	    	}
	   	}
	  
  	}

  	addPost(title: string, content: string) {
	    const postObject = {
	      id: 0,
	      title: '',
	      content: '',
	      loveIts : 0,
	      date: new Date()
	    };
	    postObject.title = title;
	    postObject.content = content;
	    postObject.id = this.posts[(this.posts.length - 1)].id + 1;
	    this.posts.push(postObject);
	    this.emitPostSubject();
  	}
  	
  	like(id: number){
  		for (var i = 0; i < this.posts.length; ++i) {
	    		 if(this.posts[i].id === id){
	    	 		this.posts[i].loveIts++;
	    	 	}
	    	}
 		
  }

  dislike(id: number){
 		for (var i = 0; i < this.posts.length; ++i) {
	    		 if(this.posts[i].id === id){
	    	 		this.posts[i].loveIts--;
	    	 	}
	    	}
  }
}
